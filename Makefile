FILES = ./build/loader.o
INCLUDES = -I ./includes
FLAGS = -L . -lbfd

all: ./build/loader.o

./build/loader.o: ./src/loader/loader.cc
	# gcc $(INCLUDES) $(FLAGS) -c ./src/loader/loader.cc -o ./build/loader.o
	gcc $(INCLUDES) $(FLAGS) ./src/loader/loader.cc -o ./bin/loader

clean:
	rm -rf ./bin/loader
	rm -rf ./build/loader.o